package main

import (
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"math/big"
)

func hash() []byte {
	clientRandom, _ := hex.DecodeString("57b3c6a014f3bf75e49d99a7502a9cc94af24b26a05d06bc2eeefebe44ac7431")
	serverRandom, _ := hex.DecodeString("e98405e4b8801a69c54f067e094f6f3dc1c4e2f3937af72d9ae4a4ddd116392c")
	serverParams, _ := hex.DecodeString("03001741046bdb6948d2ee1ad618f5718bd6f749a7f9e56109a235f3274be0a4a809d8764bc1ecd561a28b6d2cd45b2beb1570d64c2f0a4109dd1c39cbf193f855650a2d75")

	s := sha512.New384()
	s.Write(clientRandom)
	s.Write(serverRandom)
	s.Write(serverParams)

	return s.Sum(nil)
}

func gcd(a, b *big.Int) *big.Int {
	t := big.NewInt(0)
	zero := big.NewInt(0)
	for b.Cmp(zero) != 0 {
		t.Rem(a, b)
		a, b, t = b, t, a
	}
	return a
}

func main() {
	N := big.NewInt(0)
	// N = ...
	N.SetBytes([]byte("85467d0a99623e161a3127a1a0803d9eb3ca3ac924fa289e26262dc0f67ddfdeb3f24ff7593e479c41ff7372e53eebdf729010e71b496a8d3136d5a315ab8627fb02afb06ce0e44539a493fe2654ef742986bf0883c42af22b1f180b3ddbe008559924be18716658a99f649799e989dbcfab5a2c520f41904cdcb6a8ead161af07090ad9e14797b6d9ba458acef7807f6124caa03bb3a65a6794be6214e72f04bf574f7605cf04dd80165e268751096de62781ebd0edf7d05609c9002149db7a63683060706c6c348bc3fe0e77e4b16db12d8312aa1ab6e4fc43e643ac03f6a16d29412b59d247ec1e90534dad1f05949f281f9b5c13330e6e9208edac02eb1f"))

	// e = ...
	e := big.NewInt(65537)

	m := big.NewInt(0)
	// m = hash
	m.SetBytes(hash())

	faultySignature := big.NewInt(0)
	// faultySignature = ...
	faultySignature.SetBytes([]byte("85467d0a99623e161a3127a1a0803d9eb3ca3ac924fa289e26262dc0f67ddfdeb3f24ff7593e479c41ff7372e53eebdf729010e71b496a8d3136d5a315ab8627fb02afb06ce0e44539a493fe2654ef742986bf0883c42af22b1f180b3ddbe008559924be18716658a99f649799e989dbcfab5a2c520f41904cdcb6a8ead161af07090ad9e14797b6d9ba458acef7807f6124caa03bb3a65a6794be6214e72f04bf574f7605cf04dd80165e268751096de62781ebd0edf7d05609c9002149db7a63683060706c6c348bc3fe0e77e4b16db12d8312aa1ab6e4fc43e643ac03f6a16d29412b59d247ec1e90534dad1f05949f281f9b5c13330e6e9208edac02eb1f"))

	fmt.Println("calculating gcd(faultySignature^e-m, N)")
	// p = gcd(faultySignature^e-m, N)
	p := gcd(big.NewInt(0).Sub(big.NewInt(0).Exp(faultySignature, e, nil), m), N)
	fmt.Println("p", p)

	fmt.Println("calculating N/p")
	// q = N/p
	q := big.NewInt(0).Div(N, p)
	fmt.Println("q", p)

	fmt.Println("calculating (p-1)*(q-1)")
	// fiN = (p-1)*(q-1)
	fiN := big.NewInt(0).Mul(big.NewInt(0).Sub(p, big.NewInt(1)), big.NewInt(0).Sub(q, big.NewInt(1)))
	fmt.Println("fiN", fiN)

	// d = exp(e, -1, fiN)
	d := big.NewInt(0).Exp(e, big.NewInt(-1), fiN)
	fmt.Println("d", d)

	fmt.Println(d)
}
